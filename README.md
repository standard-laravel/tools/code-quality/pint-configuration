## Laravel Pint Configuration
Opinionated code style better aligned with Laravel rules and few improvements.

### Installation
```
composer require standard-laravel/tools-code-quality-pint-configuration
```

### Setup
```
php artisan vendor:publish --tag=standard:tools:code-quality:pint:configuration
```
