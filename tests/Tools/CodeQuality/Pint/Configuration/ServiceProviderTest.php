<?php

namespace Tests\Tools\CodeQuality\Pint\Configuration;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;
use Standard\Tools\CodeQuality\Pint\Configuration\Providers\ServiceProvider;
use Tests\TestCase;

class ServiceProviderTest extends TestCase
{
    public function test_it_can_publish_pint_configuration()
    {
        $class = ServiceProvider::class;

        $this->assertTrue(
            collect($class::publishableGroups())->contains('standard:tools:code-quality:pint:configuration')
        );

        collect(collect($class::$publishes)->get($class))->each(function ($destination, $source) {
            $this->assertFileExists($source);
            $this->assertSame($destination, base_path('pint.json'));
            Artisan::call('vendor:publish', ['--tag' => 'standard:tools:code-quality:pint:configuration', '--force' => true]);
            $this->assertFileExists(base_path('pint.json'));
            File::delete(base_path('pint.json'));
        });
    }

    protected function getPackageProviders($app)
    {
        return [
            ServiceProvider::class,
        ];
    }
}
