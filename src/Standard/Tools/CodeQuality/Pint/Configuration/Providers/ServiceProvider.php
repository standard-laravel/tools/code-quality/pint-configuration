<?php

namespace Standard\Tools\CodeQuality\Pint\Configuration\Providers;

class ServiceProvider extends \Standard\Library\Providers\ServiceProvider
{
    protected function publishables(): iterable
    {
        return [
            'standard:tools:code-quality:pint:configuration' => [
                __DIR__.'/../../../../../../../config/pint.json' => base_path('pint.json'),
            ],
        ];
    }
}
